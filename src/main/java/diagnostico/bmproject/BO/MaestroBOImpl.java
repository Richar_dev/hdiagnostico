package diagnostico.bmproject.BO;

import diagnostico.bmproject.ENTITY.TblMaestro;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import diagnostico.bmproject.repository.IMaestroBO;
import diagnostico.bmproject.repository.IMaestroDAO;

/**
 *
 * @author ricar
 */
@Service("maestroBO")
public class MaestroBOImpl implements IMaestroBO {

  @Autowired
  private IMaestroDAO maestroDao;

  @Override
  public List<TblMaestro> findAll() {
    return maestroDao.findAll();
  }

  @Override
  public void save(TblMaestro obj) {
    maestroDao.save(obj);
  }

  @Override
  public void update(TblMaestro obj) {
    maestroDao.update(obj);
  }

  @Override
  public void delete(TblMaestro obj) {

    maestroDao.delete(obj);
  }

  @Override
  public TblMaestro findById(TblMaestro t) {
    return maestroDao.findById(t);
  }

}
