package diagnostico.bmproject.BO;

import diagnostico.bmproject.ENTITY.TblEncabezado;
import diagnostico.bmproject.repository.IEncabezadoBO;
import diagnostico.bmproject.repository.IEncabezadoDAO;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author ricar
 */

@Service("encabezadoBO")
public class EncabezadoBOImpl implements IEncabezadoBO {
  
  @Autowired
  private IEncabezadoDAO encabezadoDAO;

  @Override
  public void save(TblEncabezado obj) {
    encabezadoDAO.save(obj);
  }

  @Override
  public void delete(TblEncabezado obj) {
    encabezadoDAO.delete(obj);
  }

  @Override
  public void update(TblEncabezado obj) {
    encabezadoDAO.update(obj);
  }

  @Override
  public List<TblEncabezado> findAll() {
    return encabezadoDAO.findAll();
  }

  @Override
  public TblEncabezado findById(TblEncabezado t) {
    throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
  }
  
}
