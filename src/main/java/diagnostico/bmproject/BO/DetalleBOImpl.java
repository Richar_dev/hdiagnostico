package diagnostico.bmproject.BO;

import diagnostico.bmproject.ENTITY.TblDetalle;
import diagnostico.bmproject.ENTITY.TblMaestro;
import diagnostico.bmproject.repository.IDetalleBO;
import diagnostico.bmproject.repository.IDetalleDAO;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author ricar
 */
@Service(value = "detalleBO")
public class DetalleBOImpl implements IDetalleBO {

  @Autowired
  private IDetalleDAO detalleDao;
  
  @Override
  public void save(TblDetalle obj) {
    detalleDao.save(obj);
  }

  @Override
  public void delete(TblDetalle obj) {
    detalleDao.delete(obj);
  }

  @Override
  public void update(TblDetalle obj) {
    detalleDao.update(obj);
  }

  @Override
  public List<TblDetalle> findAll() {
    return detalleDao.findAll();
  }

  @Override
  public TblDetalle findById(TblDetalle t) {
    throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
  }
    
}
