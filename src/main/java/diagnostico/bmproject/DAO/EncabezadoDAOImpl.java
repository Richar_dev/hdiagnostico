package diagnostico.bmproject.DAO;

import diagnostico.bmproject.ENTITY.TblEncabezado;
import diagnostico.bmproject.repository.IEncabezadoDAO;
import java.util.List;
import org.hibernate.SessionFactory;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.orm.hibernate5.support.HibernateDaoSupport;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author ricar
 */

@Repository
@Transactional(value = "TMBMP")
public class EncabezadoDAOImpl extends HibernateDaoSupport implements IEncabezadoDAO {

  public EncabezadoDAOImpl(@Qualifier("SFBMP") SessionFactory sf) {
    super.setSessionFactory(sf);
  }
  
  @Override
  public void save(TblEncabezado obj) {
    currentSession().save(obj);
  }

  @Override
  public void delete(TblEncabezado obj) {
    currentSession().delete(obj);
  }

  @Override
  public void update(TblEncabezado obj) {
    currentSession().update(obj);
  }

  @Override
  public List<TblEncabezado> findAll() {

    return currentSession()
            .createQuery("SELECT t FROM TblEncabezado t")
            .list();
  }

  @Override
  public TblEncabezado findById(TblEncabezado t) {
    throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
  }

}
