package diagnostico.bmproject.DAO;

import diagnostico.bmproject.ENTITY.TblMaestro;
import java.util.List;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.orm.hibernate5.support.HibernateDaoSupport;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import diagnostico.bmproject.repository.IMaestroDAO;

/**
 *
 * @author ricar
 */
@Repository
@Transactional(value = "TMBMP")
public class MaestroDAOImpl extends HibernateDaoSupport implements IMaestroDAO {

  public MaestroDAOImpl(@Qualifier("SFBMP") SessionFactory sf) {
    super.setSessionFactory(sf);
  }

  @Override
  public void save(TblMaestro obj) {
    currentSession().save(obj);
  }

  @Override
  public void update(TblMaestro obj) {
    currentSession().update(obj);
  }

  @Override
  public void delete(TblMaestro obj) {
    currentSession().delete(obj);
  }

  @Override
  public List<TblMaestro> findAll() {
    return currentSession()
            .createQuery("SELECT m FROM TblMaestro m")
            .list();
  }

  @Override
  public TblMaestro findById(TblMaestro m) {
    return currentSession()
            .find(TblMaestro.class, m.getCampo1());
  }

}
