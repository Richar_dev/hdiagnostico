package diagnostico.bmproject.DAO;

import diagnostico.bmproject.ENTITY.TblDetalle;
import diagnostico.bmproject.ENTITY.TblMaestro;
import diagnostico.bmproject.repository.IDetalleDAO;
import java.util.List;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.orm.hibernate5.support.HibernateDaoSupport;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author ricar
 */
@Repository
@Transactional("TMBMP")
public class DetalleDAOImpl extends HibernateDaoSupport implements IDetalleDAO {

  public DetalleDAOImpl(@Qualifier("SFBMP") SessionFactory sf) {
    super.setSessionFactory(sf);
  }

  @Override
  public void save(TblDetalle obj) {
    currentSession().save(obj);
  }

  @Override
  public void delete(TblDetalle obj) {
    currentSession().delete(obj);
  }

  @Override
  public void update(TblDetalle obj) {
    currentSession().update(obj);
  }

  @Override
  public List<TblDetalle> findAll() {
    String sql = "SELECT d FROM TblDetalle d";
    return currentSession()
            .createQuery(sql)
            .list();
  }

  @Override
  public TblDetalle findById(TblDetalle t) {
    throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
  }

}
