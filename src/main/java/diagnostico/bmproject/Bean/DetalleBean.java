package diagnostico.bmproject.Bean;

import diagnostico.bmproject.ENTITY.TblDetalle;
import diagnostico.bmproject.ENTITY.TblDetallePK;
import diagnostico.bmproject.ENTITY.TblEncabezado;
import diagnostico.bmproject.ENTITY.TblEncabezadoPK;
import diagnostico.bmproject.ENTITY.TblMaestro;
import diagnostico.bmproject.repository.IDetalleBO;
import diagnostico.bmproject.repository.IMaestroBO;
import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import lombok.Getter;
import lombok.Setter;

/**
 *
 * @author ricar
 */
@ManagedBean
@ViewScoped
@Getter
@Setter
public class DetalleBean implements Serializable {

  @ManagedProperty("#{detalleBO}")
  private IDetalleBO detalleBO;
  
  @ManagedProperty("#{maestroBO}")
  private IMaestroBO maestroBO;

  private List<TblDetalle> listaDetalle;
  private TblDetalle detalle;
  private TblDetallePK detallePK;

  private TblEncabezado encabezado;
  private TblEncabezadoPK encabezadoPK;
  private TblMaestro maestro;

  @PostConstruct
  public void init() {
    // lista de detalles
    listaDetalle = detalleBO.findAll();
    
    encabezado = new TblEncabezado();
    maestro = new TblMaestro();
    detalle = new TblDetalle();
    detallePK = new TblDetallePK();
    encabezadoPK = new TblEncabezadoPK();
  }

  public void save() {
    System.out.println("*****Entre al metodo Guardar*****");

    try {
      
      
      encabezado.setTblEncabezadoPK(encabezadoPK);
      
      detalle.setTblMaestro(maestro);
      detalle.setTblEncabezado(encabezado);
      
      detalle.setTblDetallePK(detallePK);
      
      // GUARDO TODO EL OBJETO CARGADO MANUALMENTE
      detalleBO.save(detalle);

      System.out.println("*****Se guardo Exitosamente*****");

    } catch (Exception e) {
      System.out.println("***Error en guardar*** " + e.getMessage());
      
    } finally {
      listaDetalle = detalleBO.findAll();
    }
  }

  public void update() {
    System.out.println("*****Entre al metodo Modificar*****");
    
    try {
      maestro.setCampo1(detalle.getTblDetallePK().getCampo14());
      maestro = maestroBO.findById(maestro);
      System.out.println(maestro.toString());
      detalle.setTblMaestro(maestro);
      
      // lleno las llaves de encabezado
      encabezadoPK.setCampo11(detalle.getTblDetallePK().getCampo11());
      encabezadoPK.setCampo12(detalle.getTblDetallePK().getCampo12());
      encabezadoPK.setCampo13(detalle.getTblDetallePK().getCampo13());
      
      encabezado.setTblEncabezadoPK(encabezadoPK);
      detalle.setTblEncabezado(encabezado);

      detalle.setTblDetallePK(detalle.getTblDetallePK());
      // GUARDO TODO EL OBJETO CARGADO MANUALMENTE
      detalleBO.update(detalle);

    } catch (Exception e) {
      System.out.println("***Error en Modificar*** " + e.getMessage());
    } finally {
      listaDetalle = detalleBO.findAll();
    }
  }

  public void delete() {
    System.out.println("*****Entre al metodo Borrar*****");
    try {

      detalleBO.delete(detalle);

    } catch (Exception e) {
      System.out.println("***Error en Borrar*** " + e.getMessage());
    } finally {
      listaDetalle = detalleBO.findAll();
    }
  }

  public void limpiar() {
    detalle = new TblDetalle();
  }

}
