package diagnostico.bmproject.Bean;

import diagnostico.bmproject.ENTITY.TblEncabezado;
import diagnostico.bmproject.ENTITY.TblEncabezadoPK;
import diagnostico.bmproject.repository.IEncabezadoBO;
import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;

/**
 *
 * @author ricar
 */

@ManagedBean
@ViewScoped
@Getter
@Setter
public class EncabezadoBean implements Serializable {

  @ManagedProperty("#{encabezadoBO}")
  private IEncabezadoBO encabezadoBO;

  private List<TblEncabezado> listaEncabezado;
  
  @Autowired
  private TblEncabezado encabezado;
  
  private TblEncabezadoPK encabezadoPK;

  @PostConstruct
  public void main() {
    listaEncabezado = encabezadoBO.findAll();
    encabezado = new TblEncabezado();
    encabezadoPK = new TblEncabezadoPK();
  }

  public void save() {
    System.out.println("*****ENTRO AL METODO SAVE*****");
    
    try {
      
      encabezado.setTblEncabezadoPK(encabezadoPK);
      encabezadoBO.save(encabezado);
    } catch (Exception e) {
      System.out.println("Error en Save encabezado: "+ e.getMessage());

    } finally {
      listaEncabezado = encabezadoBO.findAll();
      System.out.println("*****SE GUARDO EXITOSAMENTE*****");
    }
    
  }

  public void update() {
    System.out.println("*****ENTRO AL METODO UPDATE*****");
    try {
      encabezadoBO.update(encabezado);
    } catch (Exception e) {
      System.out.println("Error en Update encabezado: "+ e.getMessage());

    } finally {
      listaEncabezado = encabezadoBO.findAll();
      System.out.println("*****SE ACTUALIZO EXITOSAMENTE*****");
    }

  }

  public void delete() {
    System.out.println("*****ENTRO AL METODO DELETE*****");

    try {
      encabezadoBO.delete(encabezado);

    } catch (Exception e) {
      System.out.println("Error en Delete encabezado: "+ e.getMessage());

    } finally {
      listaEncabezado = encabezadoBO.findAll();
      System.out.println("*****SE ELIMINO EXITOSAMENTE*****");
    }

  }
  
  public void limpiar(){
    encabezado = new TblEncabezado();
    encabezadoPK = new TblEncabezadoPK();
  }
  
  
}

