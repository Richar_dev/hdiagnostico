package diagnostico.bmproject.Bean;

import diagnostico.bmproject.ENTITY.TblMaestro;
import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;
import diagnostico.bmproject.repository.IMaestroBO;

/**
 *
 * @author ricar
 */
@ViewScoped
@ManagedBean
@Getter
@Setter
public class MaestroBean implements Serializable {

  @Autowired
  private TblMaestro maestro;

  @ManagedProperty("#{maestroBO}")
  private IMaestroBO maestroBO;
  
  public void limpiar() {
    maestro = new TblMaestro();
  }

  private List<TblMaestro> listMaestro;

  @PostConstruct
  public void main() {
    listMaestro = maestroBO.findAll();
  }

  public void save() {
    System.out.println("*****Entro al metodo insert******");
    
    try {
      maestroBO.save(maestro);
      System.out.println("Tbl Maestro: " + " "
              + maestro.getCampo1() + " "
              + maestro.getCampo2().toString() + " " + maestro.getCampo3() + " " + maestro.getCampo4());

    } catch (Exception e) {

      System.out.println("uy yu yiu! " + e.getMessage());

    }finally {
      listMaestro = maestroBO.findAll();
    }

  }

  public void update() {
    System.out.println("*****Entro al metodo update*****");
    
    try {
      
      maestroBO.update(maestro);
      System.out.println("Tbl Maestro: " + " " + maestro.getCampo1() + " "
              + maestro.getCampo2() + " " + maestro.getCampo3() + " " + maestro.getCampo4());

    } catch (Exception e) {
      System.out.println("uy yu yiu! " + e.getMessage());
    
    }finally{
      listMaestro = maestroBO.findAll();
    } 
    
  }

  public void delete() {
    System.out.println("*****Entro al metodo delete*****");

    try {
      maestroBO.delete(maestro);
      System.out.println("Tbl Maestro :campo eliminado: ");

    } catch (Exception e) {
      System.out.println("uy yu yiu! " + e.getMessage());
    }finally {
      listMaestro = maestroBO.findAll();
    }
    
  }

}
