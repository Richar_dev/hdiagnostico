package diagnostico.bmproject.ENTITY;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import lombok.Getter;
import lombok.Setter;

/**
 *
 * @author ricar
 */
@Entity
@Table(name = "tbl_encabezado")
@Getter
@Setter
public class TblEncabezado implements Serializable {

  private static final long serialVersionUID = 1L;
  @EmbeddedId
  protected TblEncabezadoPK tblEncabezadoPK;
  
  @Size(min = 1, max = 50)
  @Column(name = "campo14")
  private String campo14;
  
  @OneToMany(cascade = CascadeType.ALL, mappedBy = "tblEncabezado")
  private List<TblDetalle> tblDetalleList;

  public TblEncabezado() {
  }

  public TblEncabezado(TblEncabezadoPK tblEncabezadoPK) {
    this.tblEncabezadoPK = tblEncabezadoPK;
  }

  public TblEncabezado(TblEncabezadoPK tblEncabezadoPK, String campo14) {
    this.tblEncabezadoPK = tblEncabezadoPK;
    this.campo14 = campo14;
  }

  public TblEncabezado(String campo11, Date campo12, int campo13) {
    this.tblEncabezadoPK = new TblEncabezadoPK(campo11, campo12, campo13);
  }

  @Override
  public int hashCode() {
    int hash = 7;
    hash = 47 * hash + Objects.hashCode(this.tblEncabezadoPK);
    return hash;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj) {
      return true;
    }
    if (obj == null) {
      return false;
    }
    if (getClass() != obj.getClass()) {
      return false;
    }
    final TblEncabezado other = (TblEncabezado) obj;
    return Objects.equals(this.tblEncabezadoPK, other.tblEncabezadoPK);
  }
  
  
    
  
  @Override
  public String toString() {
    return "diagnostico.bmproject.ENTITY.TblEncabezado[ tblEncabezadoPK=" + tblEncabezadoPK + " ]";
  }
  
}
