package diagnostico.bmproject.ENTITY;

import java.io.Serializable;
import java.util.Date;
import java.util.Objects;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;

/**
 *
 * @author ricar
 */
@Entity
@Table(name = "tbl_detalle")
@Getter
@Setter
public class TblDetalle implements Serializable {

  private static final long serialVersionUID = 1L;
  @EmbeddedId
  protected TblDetallePK tblDetallePK;

  @Size(max = 50)
  @Column(name = "campo15")
  private String campo15;

  @Size(max = 50)
  @Column(name = "campo16")
  private String campo16;

  @JoinColumns({
    @JoinColumn(name = "campo11", referencedColumnName = "campo11", insertable = false, updatable = false),
    @JoinColumn(name = "campo12", referencedColumnName = "campo12", insertable = false, updatable = false),
    @JoinColumn(name = "campo13", referencedColumnName = "campo13", insertable = false, updatable = false)})
  @ManyToOne(optional = false)
  @Cascade(CascadeType.SAVE_UPDATE)
  private TblEncabezado tblEncabezado;

  @JoinColumn(name = "campo14", referencedColumnName = "campo1", insertable = false, updatable = false)
  @ManyToOne(optional = false)
  @Cascade(CascadeType.SAVE_UPDATE)
  private TblMaestro tblMaestro;

  public TblDetalle() {
  }

  public TblDetalle(TblDetallePK tblDetallePK) {
    this.tblDetallePK = tblDetallePK;
  }

  public TblDetalle(String campo11, Date campo12, short campo13, String campo14) {
    this.tblDetallePK = new TblDetallePK(campo11, campo12, campo13, campo14);
  }

  @Override
  public int hashCode() {
    int hash = 7;
    hash = 43 * hash + Objects.hashCode(this.tblDetallePK);
    return hash;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj) {
      return true;
    }
    if (obj == null) {
      return false;
    }
    if (getClass() != obj.getClass()) {
      return false;
    }
    final TblDetalle other = (TblDetalle) obj;
    return Objects.equals(this.tblDetallePK, other.tblDetallePK);
  }

  @Override
  public String toString() {
    return "diagnostico.bmproject.ENTITY.TblDetalle[ tblDetallePK=" + tblDetallePK + " ]";
  }

}
