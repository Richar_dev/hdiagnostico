package diagnostico.bmproject.ENTITY;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import lombok.Getter;
import lombok.Setter;

/**
 *
 * @author ricar
 */
@Entity
@Table(name = "tbl_maestro")
@Getter
@Setter
public class TblMaestro implements Serializable{
  
  private static final long serialVersionUID = 1L;
  @Id
  @NotNull
  @Size(min = 1, max = 50)
  @Column(name = "campo1")
  private String campo1;
  
  @Column(name = "campo2")
  @Temporal(TemporalType.TIMESTAMP)
  private Date campo2;
  
  @Column(name = "campo3")
  private int campo3;
  
  @Size(min = 1, max = 50)
  @Column(name = "campo4")
  private String campo4;
  
  @OneToMany(cascade = CascadeType.ALL, mappedBy = "tblMaestro")
  private List<TblDetalle> tblDetalleList;

  public TblMaestro() {
  }

  public TblMaestro(String campo1) {
    this.campo1 = campo1;
  }

  public TblMaestro(String campo1, Date campo2, int campo3, String campo4) {
    this.campo1 = campo1;
    this.campo2 = campo2;
    this.campo3 = campo3;
    this.campo4 = campo4;
  }

  @Override
  public String toString() {
    return "diagnostico.bmproject.ENTITY.TblMaestro[ campo1=" + campo1 + " ]";
  }
  
}
