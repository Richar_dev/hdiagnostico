package diagnostico.bmproject.ENTITY;

import java.io.Serializable;
import java.util.Date;
import java.util.Objects;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import lombok.Getter;
import lombok.Setter;

/**
 *
 * @author ricar
 */
@Embeddable
@Getter
@Setter
public class TblEncabezadoPK implements Serializable {

  private static final long serialVersionUID = 1L;
  @NotNull
  @Size(min = 1, max = 10)
  @Column(name = "campo11")
  private String campo11; 
  
  @NotNull
  @Column(name = "campo12")
  @Temporal(TemporalType.TIMESTAMP)
  private Date campo12;
  
  @NotNull
  @Column(name = "campo13")
  private int campo13;

  public TblEncabezadoPK() {
  }

  public TblEncabezadoPK(String campo11, Date campo12, int campo13) {
    this.campo11 = campo11;
    this.campo12 = campo12;
    this.campo13 = campo13;
  }

  @Override
  public int hashCode() {
    int hash = 7;
    hash = 47 * hash + Objects.hashCode(this.campo11);
    hash = 47 * hash + Objects.hashCode(this.campo12);
    hash = 47 * hash + this.campo13;
    return hash;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj) {
      return true;
    }
    if (obj == null) {
      return false;
    }
    if (getClass() != obj.getClass()) {
      return false;
    }
    final TblEncabezadoPK other = (TblEncabezadoPK) obj;
    if (this.campo13 != other.campo13) {
      return false;
    }
    if (!Objects.equals(this.campo11, other.campo11)) {
      return false;
    }
    return Objects.equals(this.campo12, other.campo12);
  }

 
  
}
