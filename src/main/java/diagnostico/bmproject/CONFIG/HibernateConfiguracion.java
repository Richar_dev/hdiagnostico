/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package diagnostico.bmproject.CONFIG;

import static diagnostico.bmproject.CONFIG.HibernateConexiones.DB_POSTGRES_DIALECT;
import static diagnostico.bmproject.CONFIG.HibernateConexiones.DB_POSTGRES_DRIVER_CLASS;
import static diagnostico.bmproject.CONFIG.HibernateConexiones.DB_POSTGRES_PASS;
import static diagnostico.bmproject.CONFIG.HibernateConexiones.DB_POSTGRES_URL;
import static diagnostico.bmproject.CONFIG.HibernateConexiones.DB_POSTGRES_USER;

import java.util.Properties;
import javax.sql.DataSource;
import org.hibernate.cfg.Environment;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.orm.hibernate5.LocalSessionFactoryBean;
import org.springframework.jdbc.datasource.DriverManagerDataSource;

import org.springframework.orm.hibernate5.HibernateTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;


/**
 *
 * @author Administrador
 */
@Configuration
@EnableTransactionManagement
@ComponentScan(
        basePackages = {
            "diagnostico.bmproject.BO",
            "diagnostico.bmproject.DAO"
        })
public class HibernateConfiguracion {

    private DriverManagerDataSource dataSource;
    private LocalSessionFactoryBean sessionFactory;
    private HibernateTransactionManager transactionManager;

    private Properties hibPropertiesPostgres() {
        Properties props = new Properties();
        // Setting Hibernate properties
//        props.put(Environment.DIALECT, DB_POSTGRES_DIALECT);
        props.put(Environment.DIALECT, "org.hibernate.dialect.PostgreSQL95Dialect");
        props.put(Environment.SHOW_SQL, false);
        props.put(Environment.FORMAT_SQL, true);
        props.put(Environment.ENABLE_LAZY_LOAD_NO_TRANS, true);
        return props;
    }

    
    
    //************************************************************************
    //*************************** BMPPROJECT ********************************
    //************************************************************************
    @Bean(name = "DSBMP")
    public DataSource DSBMP() {
        dataSource = new DriverManagerDataSource();
//        dataSource.setDriverClassName(DB_POSTGRES_DRIVER_CLASS);
        dataSource.setDriverClassName("org.postgresql.Driver");
        dataSource.setUrl(DB_POSTGRES_URL);
        dataSource.setUsername(DB_POSTGRES_USER);
        dataSource.setPassword(DB_POSTGRES_PASS);
        return dataSource;
    }

    @Bean(name = "SFBMP")
    public LocalSessionFactoryBean SFBMP() {
        sessionFactory = new org.springframework.orm.hibernate5.LocalSessionFactoryBean();
        sessionFactory.setDataSource(DSBMP());
        sessionFactory.setHibernateProperties(hibPropertiesPostgres());
        sessionFactory.setPackagesToScan(new String[]{
                "diagnostico.bmproject.ENTITY"
        });
        return sessionFactory;
    }

    @Bean(name = "TMBMP")
    public HibernateTransactionManager TMBMP() {
        transactionManager = new org.springframework.orm.hibernate5.HibernateTransactionManager();
        transactionManager.setSessionFactory(SFBMP().getObject());
        return transactionManager;
    }

}
