/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package diagnostico.bmproject.CONFIG;

import com.sun.faces.config.ConfigureListener;
import javax.faces.webapp.FacesServlet;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRegistration.Dynamic;
import org.springframework.web.WebApplicationInitializer;
import org.springframework.web.context.ContextLoaderListener;
import org.springframework.web.context.request.RequestContextListener;
import org.springframework.web.context.support.AnnotationConfigWebApplicationContext;

/**
// *
 * @author Administrador
 */
public class WebAppInitializer implements WebApplicationInitializer {

    @Override
    public void onStartup(ServletContext sc) throws ServletException {
        setupSpringApplicationContext(sc);
        setupFaceContext(sc);
    }

    private void setupSpringApplicationContext(ServletContext servletContext) {
        AnnotationConfigWebApplicationContext ctx = new AnnotationConfigWebApplicationContext();

        ctx.register(HibernateConfiguracion.class);             
        ctx.setServletContext(servletContext);

        servletContext.addListener(new ContextLoaderListener(ctx));
        servletContext.addListener(new RequestContextListener());
    }

    private void setupFaceContext(ServletContext servletContext) {
         /**
         * Faces Servlet
         */
        // Use JSF view templates saved as *.xhtml, for use with Facelets
////		servletContext.setInitParameter("javax.faces.DEFAULT_SUFFIX", ".xhtml");
//        servletContext.setInitParameter("javax.faces.DEFAULT_SUFFIX", ".hospital");

        // Enable special Facelets debug output during development(Development , Production)
        // Habilita la salida de depuración de Facelets especiales durante el desarrollo
        servletContext.setInitParameter("javax.faces.PROJECT_STAGE", "Development");
 
        // Causes Facelets to refresh templates during development
        // Hace que Facelets actualice plantillas durante el desarrollo
        servletContext.setInitParameter("javax.faces.FACELETS_REFRESH_PERIOD", "1");

        //THEME
//        servletContext.setInitParameter("primefaces.THEME", "hospital-v1");

        // Controls client side validation.
        // Controla la validación del lado del cliente.
        servletContext.setInitParameter("primefaces.CLIENT_SIDE_VALIDATION", "false");

        // Enabled font-awesome icons. (default : false)
        // Habilitado los iconos de fuente impresionante. (predeterminado: falso)
        servletContext.setInitParameter("primefaces.FONT_AWESOME", "true");

        servletContext.setInitParameter("javax.faces.STATE_SAVING_METHOD", "server");
        servletContext.setInitParameter("org.apache.myfaces.SERIALIZE_STATE_IN_SESSION", "false");
        servletContext.setInitParameter("javax.faces.FACELETS_BUFFER_SIZE", "65535");
        servletContext.setInitParameter("com.ocpsoft.pretty.CONFIG_FILES", "/WEB-INF/faces-config.xml");
        
        servletContext.setInitParameter("facelets.SKIP_COMMENTS", "true");
        servletContext.setInitParameter("javax.faces.FACELETS_SKIP_COMMENTS", "true");
         
//        ServletRegistration.Dynamic facesServlet = servletContext.addServlet("Faces Servlet", FacesServlet.class);
        Dynamic facesServlet = servletContext.addServlet("Faces Servlet", new FacesServlet());
        facesServlet.addMapping("*.hospital");
        facesServlet.setLoadOnStartup(-1);


        servletContext.addListener(ConfigureListener.class);
    }

}
