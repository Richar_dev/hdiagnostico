/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package diagnostico.bmproject.repository;

import java.util.List;

/**
 *
 * @author ricar
 */
public interface GenericCRUD<T> {
 
  void save(T obj);
  void delete(T obj);
  void update(T obj);
  
  List<T> findAll();
  
  T findById(T t);
  
}
