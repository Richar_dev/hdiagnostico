package diagnostico.bmproject.repository;

import diagnostico.bmproject.ENTITY.TblMaestro;

/**
 *
 * @author ricar
 */
public interface IMaestroBO extends GenericCRUD<TblMaestro> {

}
