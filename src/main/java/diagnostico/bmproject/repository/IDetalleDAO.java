
package diagnostico.bmproject.repository;

import diagnostico.bmproject.ENTITY.TblDetalle;
import diagnostico.bmproject.ENTITY.TblMaestro;

/**
 *
 * @author ricar
 */
public interface IDetalleDAO extends GenericCRUD<TblDetalle>{
  
  
}
