package diagnostico.bmproject.repository;

import diagnostico.bmproject.ENTITY.TblEncabezado;

/**
 *
 * @author ricar
 */
public interface IEncabezadoDAO extends GenericCRUD<TblEncabezado> {
  
}
