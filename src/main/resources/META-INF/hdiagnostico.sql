/*CREATE DATABASE hdiagnostico
    WITH 
    OWNER = postgres
    ENCODING = 'UTF8'
    CONNECTION LIMIT = -1;*/

CREATE TABLE tbl_maestro(
	campo1 VARCHAR(50) NOT NULL,
	campo2 TIMESTAMP,
	campo3 SMALLINT,
	campo4 VARCHAR(50),
	CONSTRAINT pk_maestro_campo1 PRIMARY KEY (campo1)
);

CREATE TABLE tbl_encabezado(
	campo11 VARCHAR(10) NOT NULL,
	campo12 TIMESTAMP NOT NULL,
	campo13 SMALLINT NOT NULL,
	campo14 VARCHAR(50),
	CONSTRAINT pk_encabezado PRIMARY KEY (campo11, campo12, campo13)
);

CREATE TABLE tbl_detalle(
	campo11 VARCHAR(10) NOT NULL,
	campo12 TIMESTAMP NOT NULL,
	campo13 SMALLINT NOT NULL,
	campo14 VARCHAR(50) NOT NULL,
	campo15 VARCHAR(50),
	campo16 VARCHAR(50),
	CONSTRAINT pk_detalle PRIMARY KEY (campo11,campo12,campo13,campo14),
	
	CONSTRAINT fk_detalle_encabezado FOREIGN KEY (campo11,campo12,campo13) REFERENCES tbl_encabezado(campo11,campo12,campo13),
	CONSTRAINT fk_detalle_maestro_c1 FOREIGN KEY (campo14) REFERENCES tbl_maestro(campo1)
);

-- insersiones de pruebas, DEBE DE IR UNO POR UNO, YA QUE LA FECHA ES PRIMARY KEY, DEBE SER DIFERENTE
INSERT INTO tbl_maestro VALUES('MID1',NOW(), 1, 'CAMPO4 TBL_MAESTRO');
INSERT INTO tbl_maestro VALUES('MID2',NOW(), 2, 'CAMPO4 TBL_MAESTRO');
INSERT INTO tbl_maestro VALUES('MID3',NOW(), 3, 'CAMPO4 TBL_MAESTRO');
INSERT INTO tbl_maestro VALUES('MID4',NOW(), 4, 'CAMPO4 TBL_MAESTRO');
INSERT INTO tbl_maestro VALUES('MID5',NOW(), 5, 'CAMPO4 TBL_MAESTRO');
								
INSERT INTO tbl_encabezado VALUES('ezdo1', NOW(), 1, 'campo14 tbl_encabezado');
INSERT INTO tbl_encabezado VALUES('ezdo2', NOW(), 2, 'campo14 tbl_encabezado');
INSERT INTO tbl_encabezado VALUES('ezdo3', NOW(), 3, 'campo14 tbl_encabezado');
INSERT INTO tbl_encabezado VALUES('ezdo4', NOW(), 4, 'campo14 tbl_encabezado');
INSERT INTO tbl_encabezado VALUES('ezdo5', NOW(), 5, 'campo14 tbl_encabezado');


INSERT INTO tbl_detalle VALUES('ezdo1', '2022-04-15 15:00:09.867363', 1, 'MID1', 'tabla de detalles campo15', 'tabla de detalles campo16');

SELECT d.campo11, d.campo12, d.campo13, d.campo14, d.campo15, d.campo16
FROM tbl_detalle AS d INNER JOIN tbl_encabezado AS e 
ON d.campo11 = e.campo11 AND d.campo12 = e.campo12 AND d.campo13 = e.campo13
INNER JOIN tbl_maestro AS ma 
ON d.campo14 = ma.campo1;

select * from tbl_encabezado;
select * from tbl_detalle;
select * from tbl_maestro;

UPDATE tbl_detalle SET campo15 = 'tabla de detalles campo15';
DELETE FROM tbl_encabezado WHERE campo11 = 'ezdo1' ;









